const express = require('express')//library
const app = express() //กำลังจะเรียกใช้ฟังชั่น()

let todos = [
    {
        name: 'nut',
        id: 1
    },
    {
        name: 'nuttamon',
        id: 2
    }
]

//select * from todo//1.applicatoin 2.ตามด้วยแอคชั่น 3.ตามด้วยชื่อที่กำลังจะทำ
app.get('/todos',(req,res) => {
    res.send(todos)
})
//insert into todoเพิ่มข้อมูล
app.post('/todos',(req,res) => {
    let newTodo = {
        name: 'Read s book',
        id: 3
    }
    todos.push(newTodo)
    //todosอาเรย์ข้างบน
    res.status(201).send()
})

app.listen(3000, () =>{
    console.log('TODO API Started at port 3000')
})